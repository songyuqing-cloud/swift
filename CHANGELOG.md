# Changelog

## 1.1.0

* aggiunta immagine swift 5.0
* aggiunta immagine swift 5.1
* aggiunta immagine swift 5.2
* aggiunta immagine swift 5.3
* aggiunta immagine swift 5.4

## 1.0.0

* prima versione
