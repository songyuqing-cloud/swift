FROM kylef/swiftenv
ARG SWIFT_NEW_VERSION=5.0
RUN apt-get update && apt-get install jq -y
RUN swiftenv install ${SWIFT_NEW_VERSION}
